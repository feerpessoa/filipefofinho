/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.test.patterns;

import br.com.patterns.FabricaModoComer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author root
 */
public class TestFabricaModoComer {
    
    public TestFabricaModoComer() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testaSeRetornaMesmaInstancia() {
        FabricaModoComer fabrica = FabricaModoComer.getInstance();
        FabricaModoComer fabricaDois = FabricaModoComer.getInstance();
        assertEquals(fabrica, fabricaDois);
    }
}
