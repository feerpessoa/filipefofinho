package br.com.patterns;

public class Humano implements HumanoObserver {

    private Comportamento comportamento;

    public void mudarModoComer() {
        comportamento.modoComer();
    }

    public void setComportamento(Comportamento comportamento) {
        this.comportamento = comportamento;
    }

    @Override
    public void tipoModoComer(String nacionalidade) {
        FabricaModoComer fabricaModoComer = FabricaModoComer.getInstance();
        Comportamento comportamento = fabricaModoComer.getComportamento(nacionalidade);
        setComportamento(comportamento);
    }
}
