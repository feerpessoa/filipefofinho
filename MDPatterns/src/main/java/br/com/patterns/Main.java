package br.com.patterns;

import javax.swing.JOptionPane;

public class Main {

    public static void main(String[] args) {
        Nacionalidade nacionalidade = new Nacionalidade();
        Humano humano = new Humano();
        nacionalidade.adicionaHumano(humano);        
        String modoComer = JOptionPane.showInputDialog("Qual seu modo de comer?");
        humano.tipoModoComer(modoComer);
        humano.mudarModoComer();
    }
    
}
