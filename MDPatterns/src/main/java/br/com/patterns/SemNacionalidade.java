package br.com.patterns;

public class SemNacionalidade implements Comportamento {

    @Override
    public void modoComer() {
        System.out.println("Sou um ser humano sem nacionalidade e nem como...");
    }

}
