package br.com.patterns;

public class FabricaModoComer {

    private static FabricaModoComer instancia = null;

    private FabricaModoComer() {
        System.out.println("Nacionalidade e modo de comer: ");
    }

    public static FabricaModoComer getInstance() {
        if (instancia == null) {
            instancia = new FabricaModoComer();
        }
        return instancia;
    }

    public Comportamento getComportamento(String comportamento) {
        if (comportamento.equalsIgnoreCase("Mão")) {
            return new Japones();
        }
        if (comportamento.equalsIgnoreCase("Garfo")) {
            return new Brasileiro();
        }
        return new SemNacionalidade();
    }
}
