package br.com.patterns;

import java.util.ArrayList;
import java.util.List;

public class Nacionalidade {

    List<HumanoObserver> listaDeHumanos = new ArrayList();
    private String nacionalidade;

    public void notificarTodos() {

        for (HumanoObserver humanoObserver : listaDeHumanos) {
            humanoObserver.tipoModoComer(nacionalidade);
        }
    }

    public void adicionaHumano(HumanoObserver humanoObserver) {
        listaDeHumanos.add(humanoObserver);
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
        notificarTodos();
    }
}
